import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

import Welcome from '../components/Welcome';

export default function Home(){
	return(
		<>
			<Welcome kahitAno="Jane" age="25" nickname="tarzan"/>
			<Banner />
			<Highlights />
			
		</>

		)
}