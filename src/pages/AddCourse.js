//Activity
//Add a page called AddCourse.js
//Use the route in API to create a course
//The admin can only add a course
//In the navbar, if the user is logged in.
    //add a conditional rendering where if the user is admin, the button will be Home, Courses, Add Course and Logout only.
    //if the user is not an admin, the button will be Home, Courses and Logout only
import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';

export default function AddCourse(){

	const history = useHistory();

	const { user } = useContext(UserContext);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const [isActive, setIsActive] = useState(true);

	useEffect(()=>{
		if(name !== '' && description !== '' && price !== 0){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [name, description, price])


	function addCourse(e){
		e.preventDefault();

		fetch('http://localhost:4000/courses/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
					title: 'Course Created',
					icon: 'success'
				})
				history.push('/courses')
			}else{
				Swal.fire({
					title: 'Course Creation Failed',
					icon: 'error'
				})
			}
		})

		setName('');
		setDescription('');
		setPrice(0);
	}


	return(
		<>
			<h1>Create Course</h1>
			<Form onSubmit={ e => addCourse(e)}>
				<Form.Group>
					<Form.Label>Course Name:</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter Name of the course"
						value={name}
						onChange={(e) => setName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter Description"
						value={description}
						onChange={(e) => setDescription(e.target.value)}
						required
					/>
				</Form.Group>


				<Form.Group>
					<Form.Label>Price:</Form.Label>
					<Form.Control
						type="number"
						value={price}
						onChange={(e) => setPrice(e.target.value)}
						required
					/>
				</Form.Group>

				{ isActive ? 
					<Button type="submit" variant="primary">Submit</Button>
					:
					<Button type="submit" variant="primary" disabled>Submit</Button>
				}
				
				
			</Form>
		</>
		)
}