//Activity
/*1. Create NotFound.js inside a page when the user goes to a route that is not included in the Switch component, it should display "Page Not Found/Error 404"  "Go back to the homepage(naka link sa home)"

2. Push your file to gitlab
3. Copy and paste the link to boodle named: 48= React js - Routing and Conditional Rendering*/

import React from 'react';
import { Link } from 'react-router-dom';
import { Container } from 'react-bootstrap';

export default function NotFound() {
	return(

		<Container>
			<h3> Page Not Found </h3>
			<h5>Error 404</h5>
			<p>Go back to the <Link to="/">homepage</Link>.</p>
		</Container>
		)
}
