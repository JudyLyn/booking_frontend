import React, { useState, useEffect } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';


export default function UpdateButton({courseId}) {

	const history = useHistory();
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	//forms
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const [isActive, setIsActive] = useState(true);

	useEffect(()=>{
		if(name !== '' && description !== '' && price !== 0){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [name, description, price])


	function updateCourse(){
		fetch(`http://localhost:4000/courses/${courseId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Course Created',
					icon: 'success',
					text: 'Successful Course Creation'
				})

				.then(()=>history.go(0))
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Course Creation Failed.'
				})
			}
		})
	}


	return(
		<>
			<Button variant="primary" onClick={handleShow}>Update</Button>


			<Modal show={show} onHide={handleClose}>
		        <Modal.Header closeButton>
		          <Modal.Title>Update a Course</Modal.Title>
		        </Modal.Header>

		        <Modal.Body>
		        	<Form>
		        		<Form.Group>
		        			<Form.Label>Name:</Form.Label>
		        			<Form.Control 
		        				type="text"
		        				placeholder="Enter Updated Course Name"
		        				value={name}
		        				onChange={(e)=> setName(e.target.value)}
		        				required
		        				/>
		        		</Form.Group>

		        		<Form.Group>
		        			<Form.Label>Description:</Form.Label>
		        			<Form.Control 
		        				type="text"
		        				placeholder="Enter Updated Description"
		        				value={description}
		        				onChange={(e)=> setDescription(e.target.value)}
		        				required
		        				/>
		        		</Form.Group>


		        		<Form.Group>
		        			<Form.Label>Price:</Form.Label>
		        			<Form.Control 
		        				type="number"
		        				value={price}
		        				onChange={(e)=> setPrice(e.target.value)}
		        				required
		        				/>
		        		</Form.Group>
		        	</Form>






		        </Modal.Body>

		        <Modal.Footer>
		          <Button variant="secondary" onClick={handleClose}>
		            Close
		          </Button>
		         
		        { 
		        	isActive ? 
					<Button type="submit" variant="primary" onClick={updateCourse}>Save Changes</Button>
					:
					<Button type="submit" variant="primary" disabled>Save Changes</Button>
				}


		        </Modal.Footer>
		    </Modal>
		</>
		)
}