import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';

export default function DeleteButton({courseId}){

	const history = useHistory();

	function deleteCourse(e) {
		e.preventDefault();

		fetch(`http://localhost:4000/courses/${courseId}/archive`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Successful',
					icon: 'success',
					text: 'The course is deleted'
				})

				/*window.location.reload()*/
				.then(()=>history.go(0))
			}else{
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Failed to delete a course'
				})
			}

		})
	}

	return(
		<Button variant="danger" onClick={deleteCourse}>Delete</Button>
		)
}